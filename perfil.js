$.getScript("sha256.js");
cargar();

function cargar(){
	$.ajaxSetup({
                    type: "POST",
                    data: {},
                    dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
                });
	$.ajax({
		url : "http://api.local/Clientes/estado",
		type: "POST",
		data: {},
		dataType:"json",
		success:function(response){
			if (response){
				window.loggeado = true;
				cargarperfil();
			}
			else{
				window.loggeado = false;
			}
		},
		error: function(xhr, status, error) {	
	        alert(xhr.responseText);
	        return false;
		}
	});
}

function cargarperfil() {

	$.ajaxSetup({
		type: "POST",
		data: {},
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true
	});
	$.ajax({
		url : "http://api.local/Clientes/getPerfil",
		type: "POST",
		data: {},
		dataType:"json",
		success:function(response){
			//console.log(response);
			document.getElementById('formulariologin').style.width = '300px';
			/**/
			window.saldousuario = response.saldo;
			var perfil ='<div class="text-center">';
			perfil     +='<big>Informacion del usuario</big>';
			perfil     +='</div>';
			perfil     +='<div class="text-left" style="margin-top: 30px;">';
			perfil     +='Id : '+response.idusuario;
			perfil     +='<br>Email : '+response.mail;
			perfil     +='<br> Saldo disponible : '+response.saldo;
			perfil     +='</div>';
			perfil     +='<div class="modal-footer">';
			perfil     +='<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="logout()">Logout</button>';
			perfil     +='<a href="/perfil.html">Ir al perfil</a>';
			perfil     +='</div>';
			//perfil     +="<br><br><a href='/perfil.html'>Ir al perfil</a>";
			//perfil     +="<br><br><button type='button' name='logout' onclick='logout()'>Logout</button>";
			$("#formulariologin").html(perfil);
			$("#textobotoncito").html("Informacion");
		},
		error: function(xhr, status, error) {
			alert(xhr.responseText);
		}
	});
}

function logout() {
	$.ajaxSetup({
		type: "POST",
		data: {},
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true
	});
	$.ajax({
		url : "http://api.local/Clientes/logout",
		type: "POST",
		data: {},
		dataType:"json",
		success:function(response){
			console.log(response);
			if (response) {
				//window.location.href = "/eventos.html";
				location.reload();
			}
		},
		error: function(xhr, status, error) {
			alert(xhr.responseText);
		}
	});
}

function login() {
	let mail     = document.querySelector("#Login input[name=mail]").value;
	let password = document.querySelector("#Login input[name=password]").value;
	let token = sha256(mail+":"+password);
	console.log("Token"+token);
	$.ajaxSetup({
		type: "POST",
		data: {},
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true
	});

	$.ajax({
		url : "http://api.local/Clientes/login",
		type: "POST",
                    data: {token:token}, // Nombre de variable que recibe el controller : nombre de variable que estoy pasando 
                    dataType:"json",
                    success:function(response){
                    	console.log(response);
                    	if (response.estado) {
                    		//window.location.href = "/perfil.html";
                    		location.reload();
                    	}
                    	else{
                    		alert(response.info);
                    	}
                    },
                    error: function(xhr, status, error) {
                    	alert(xhr.responseText);
                    }
                });
}

