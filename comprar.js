function comprar() {
	fecha = $("#selecttickets").val();
	saldo = saldousuario;
	metododepago = $("#metodosdepago").val();

	var elemento = document.getElementById("metodosdepago");
	var nombremetododepago = elemento.options[elemento.selectedIndex].innerHTML;

	if (typeof descuentoscargados !== 'undefined'){
		compra = verificaritems(ticketscargados,comidas,descuentoscargados,metododepago,fecha);
	}else{
		compra = verificaritems(ticketscargados,comidas,[],metododepago,fecha);
	}
	

	//Se procesan los items seleccionados para ver si hay errores o esta todo en orden con ellos
	if (Object.keys(compra["Errores"]).length == 0){
		subtotal=calcularSubtotal(compra);
		if (subtotal > 0){
			if(subtotal > saldo){
				alert("Saldo insuficiente: El subtotal es de "+subtotal+" y su saldo es de "+saldo+".");
			}
			else{
				confirmar(compra,fecha,nombremetododepago);	
			}
		}else{
			alert("El subtotal es 0, no se pueden realizar compras vacias.");
		}
			
	}else{
		var mensaje = "Se encontraron errores al procesar los items elegidos: \n\n";
		$.each(compra["Errores"],function(titulo,descripcion){
			mensaje += titulo;
			mensaje += " : ";
			mensaje += descripcion;
			mensaje += "\n\n";
		});
		alert(mensaje);
	}
}

function verificaritems(tickets,comidas,paquetes,idmetododepago,fecha){
	compra =  new Object();
	errores =  new Object(); //Aca se van a guardar los errores que se detecten en la compra

	compra.paquetes = [];
	compra.comidas = [];
	compra.tickets = [];

	$.each(paquetes,function(index,paquete){
		if(paquete.cantidadelegida != undefined){
			if (paquete.cantidadelegida >= 0 && paquete.cantidadelegida <= paquete.stock){

				if (paquete.baja == 1){
					errores["Paquete "+paquete.idpaquete+" dado de baja"] = "El paquete "+paquete.idpaquete+" parece estar dado de baja.";
				}
				if(paquete.fecha != fecha){
					errores["Paquete "+paquete.idpaquete+" fecha erronea"] = "El paquete "+paquete.idpaquete+" es para la fecha "+paquete.fecha+" y la fecha seleccioanda es "+fecha+".";
				}
				var coincidemetodo = false;
				$.each(paquete.metodosdepago,function(index,metodosdepago){
					if(metodosdepago.idmetodopago == idmetododepago){
						coincidemetodo = true;
					}
				});
				if(!coincidemetodo){
					errores["Paquete "+paquete.idpaquete+" metodo de pago erroneo"] = "El paquete "+paquete.idpaquete+" parece no tener el metodo de pago seleccionado registrado.";
				}
				if (paquete.cantidadelegida > 0){
					compra.paquetes.push(paquete);
				}
				paquete.tipotickets = [];	
			}else{
				errores["Paquete "+paquete.idpaquete+" cantidad erronea"] = "El paquete "+paquete.idpaquete+" tiene una cantidad elegida que supera el stock.";
			}

		}
	});

	$.each(comidas,function(index,comida){
		if(comida.cantidad != undefined){
			if (comida.cantidad >= 0 && comida.cantidad <= 99) {
				if (comida.baja == 1){
					errores["Comida "+comida.idcomida+" dada de baja"] = "La comida "+comida.idcomida+" parece estar dada de baja.";
				}
				if(comida.cantidad > 0){
				compra.comidas.push(comida);
				}
			}else{
				errores["Comida "+comida.idcomida+" cantidad erronea"] = "La comida "+comida.idcomida+" tiene una cantidad elegida que supera 99 o es menor a 0.";
			}
			
		}
	});
	//console.log(tickets);
	//If individuales = 0 - > No aparecer individuales. If 
	$.each(tickets,function(index,tipoticket){ 
		//Verifico que el ticket tenga pedidos o individuales o por paquete
		if(tipoticket.individuales != undefined || tipoticket.cantidadelegidaspaquete != undefined){
			if (tipoticket.baja == 1){ //Verifico que no este dado de baja
				errores["Ticket "+tipoticket.idtipoticket+" dado de baja"] = "El ticket "+tipoticket.idtipoticket+" parece estar dado de baja.";
			}
			//Verifico que la cantidad de tickets individuales + pedidos por paquete sea menor o igual al stock del tipo de ticket
			var disponibles = tipoticket.disponibles;
			if (tipoticket.individuales != undefined){
				disponibles -= tipoticket.individuales;
			}
			if (tipoticket.cantidadelegidaspaquete != undefined){
				$.each(tipoticket.cantidadelegidaspaquete,function(idpaquete,cantidad){
					disponibles -= cantidad;
				});
			}
			if(disponibles<0){
				errores["Ticket "+tipoticket.idtipoticket+" supera el stock"] = "El ticket "+tipoticket.idtipoticket+" se supera el stock.";
			}
			//Si el asiento es numerado tengo que verificar que este todo en orden con los numeros de asiento encargados.
			if (tipoticket.numerado == 1){
				var elegidas = [];

				//Verifico que la cantidad de asientos individuales ordenados sea la isma cantidad de asientos elegidos sueltos.
				if (tipoticket.individuales != undefined) {

					if (Object.keys(tipoticket.elegidassueltas).length != tipoticket.individuales){
						errores["Ticket "+tipoticket.idtipoticket+" error al elegir asientos"] = "El ticket "+tipoticket.idtipoticket+" tiene ordenadas "+tipoticket.individuales+" entradas pero tiene "+Object.keys(tipoticket.elegidassueltas).length+" asientos elegidos.";
					}

					//Para cada una de las elegidas sueltas verifico que el valor este dentro del rango aceptable
					$.each(tipoticket.elegidassueltas,function(idform,numeroasiento){
						if (numeroasiento <= 0 || numeroasiento > tipoticket.cantidad){
							errores["Ticket "+tipoticket.idtipoticket+" error al elegir asientos"] = "El numero de asiento es menor a 0 o supera el valor "+tipoticket.cantidad;
						}
						elegidas.push(numeroasiento);
					});
				}
				//Verifico que la cantidad de asientos ordenados de cada paquete sea la misma cantidad de asientos elegidos para dicho paquete.
				if (tipoticket.cantidadelegidaspaquete != undefined) {
					$.each(tipoticket.cantidadelegidaspaquete,function(idpaquete,cantidad){
						if (Object.keys(tipoticket.elegidasPaquete[idpaquete]).length != cantidad) {
							errores["Ticket "+tipoticket.idtipoticket+" error al elegir asientos"] = "El ticket "+tipoticket.idtipoticket+" tiene ordenadas "+cantidad+" entradas de paquete "+idpaquete+" pero tiene "+Object.keys(tipoticket.elegidasPaquete[idpaquete]).length+" asientos elegidos de dicho paquete.";
						}
						//Para cada una de las elegidas por paquete verifico que la misma este dentro del rango aceptable
						$.each(tipoticket.elegidasPaquete[idpaquete],function(idform,numeroasiento){
							if (numeroasiento <= 0 || numeroasiento > tipoticket.cantidad){
								errores["Ticket "+tipoticket.idtipoticket+" error al elegir asientos"] = "El numero de asiento es menor a 0 o supera el valor "+tipoticket.cantidad;
							}
							elegidas.push(numeroasiento);
						});
					});
				}
			var numerosduplicados = verificarDuplicadas(tipoticket,elegidas);

			if (numerosduplicados.length > 0){
				//console.log(numerosduplicados);
				errores["Ticket "+tipoticket.idtipoticket+" tiene asientos duplicados"] = "El ticket "+tipoticket.idtipoticket+" tiene asientos duplicados: "+numerosduplicados.toString();;
			}
			compra.tickets.push(tipoticket);
			}else{
				compra.tickets.push(tipoticket);
			}
			tipoticket.vendidas = [];
		}
	});
	compra["Errores"] = errores;
	return compra;
}

function verificarDuplicadas(tipoticket,elegidas){
	duplicadas = [];
	vendidas = [];
	if (tipoticket.vendidas.vacio == true){
		//console.log("El ticket no tiene entradas vendidas previamente.");
	}else{
		$.each(tipoticket.vendidas,function(index,objeto){ //el objeto tiene el idtipoticket y el numero de asiento vendido
			vendidas.push(parseInt(objeto.numero));
		});
	}
	for (var i = elegidas.length - 1; i >= 0; i--) {
		//console.log("Para "+elegidas[i])
		for (var e = elegidas.length - 1; e >= 0; e--) {
			if (e<i) {
				//console.log("comparar con "+elegidas[e])
				if (elegidas[e] == elegidas[i]) {
					duplicadas.push(elegidas[i]);
				}
			}
		}
	}
	for (var i = elegidas.length - 1; i >= 0; i--) {
		for (var e = vendidas.length - 1; e >= 0; e--) {
			if (elegidas[i] == vendidas[e]) {
				duplicadas.push(elegidas[i]);
			}
		}
	}
	return duplicadas;
}

function calcularSubtotal(items){
	var subtotal = 0;
	$.each(items.paquetes,function(index,paquete){
		subtotal +=paquete.precio*paquete.cantidadelegida;
	});
	$.each(items.comidas,function(index,comida){
		subtotal +=comida.precio*comida.cantidad;
	});
	$.each(items.tickets,function(index,ticket){
		if (ticket.individuales != undefined) {
			subtotal +=ticket.precio*ticket.individuales;
		}
	});
	return Math.ceil(subtotal);
}

function confirmar(items,fecha,nombremetododepago){
	var mensaje = "El subtotal es de : "+subtotal+", el saldo disponible es "+saldo+" \n¿Realizar la compra?\n\n";
	mensaje += "Desglose de items :\n" ;


	$.each(items.paquetes,function(index,paquete){
		mensaje +="\nPaquete ID: "+paquete.idpaquete;
		mensaje +=" cantidad : "+paquete.cantidadelegida+"\n";
	});

	$.each(items.comidas,function(index,comida){
		mensaje +="\nComida ID: "+comida.idcomida;
		mensaje +=" cantidad : "+comida.cantidad+"\n";
	});
	//console.log(items.tickets);
	$.each(items.tickets,function(index,ticket){
		mensaje +="\nTicket ID: "+ticket.idtipoticket+"\n";
		if (ticket.individuales != undefined) {
			mensaje +="Cantidad de entradas individuales : "+ticket.individuales;
			if (ticket.numerado == 1) {
				asientos = [];
				$.each(ticket.elegidassueltas,function(idform,numeroasiento){
					asientos.push(parseInt(numeroasiento));
				});
				mensaje +="\nNumeros de asiento elegidos: "+asientos.toString();
			}
			mensaje +="\n";
		}
		if (ticket.cantidadelegidaspaquete != undefined) {
			$.each(ticket.cantidadelegidaspaquete,function(idpaquete,cantidad){
				mensaje +="\n "+cantidad+" entradas del paquete ID: "+idpaquete+"  \n";
			});
		}
		if (ticket.elegidasPaquete != undefined) {
			$.each(ticket.elegidasPaquete,function(idpaquete,seleccionadas){
				asientos = [];
				mensaje +="\nNumeros de asiento elegidos del paquete ID: "+idpaquete+" :\n";
				$.each(seleccionadas,function(idform,numeroasiento){
					asientos.push(parseInt(numeroasiento));
				});
				mensaje +="   "+asientos.toString()+"\n";
			});
		}
	});
	mensaje += "\nPara la fecha: "+fecha;
	mensaje += "\n";
	mensaje += "Con el metodo de pago: "+nombremetododepago;
	if(confirm(mensaje)){
		alert("Enviando compra");
		enviar(items,fecha,metododepago);
	}else{
		 alert("Compra cancelada");
	}
}

function enviar(items,fecha,metododepago){
	/*
		console.log(items);
		console.clear();
		console.log(fecha);
		console.log(metododepago);
		console.log(idevento);
	*/
	$.ajaxSetup({
		type: "POST",
		data: {},
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true
	});
	$.ajax({
		url : "http://api.local/eventos/comprar",
		type: "POST",
		data: {fecha:fecha,idevento:idevento,metododepago:metododepago,items:items},
		dataType:"json",
		success:function(response){
			/*
			console.log(response);
			*/
			if (response.exito == true) {
				alert(response.info);
				window.location.href = "/eventos.html";
			}else{
				var mensaje = "";
				$.each(response.errores,function(titulo,descripcion){
					mensaje += "\n"+titulo+": ";
					mensaje += descripcion+"\n";
				});
				alert(mensaje);
			}
		},error: function(xhr, status, error) {
			console.log(xhr.responseText);
		}
	});
}