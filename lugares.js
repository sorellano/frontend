$(document).on("ready", lugares);
	function lugares() {
       	$.ajaxSetup({
            type: "POST",
            data: {},
            dataType: 'json',
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true
            });
        $.ajax({
            url : "http://api.local/lugar/listar",
            type: "POST",
            data: {},
            dataType:"json",
            success:function(response){
                var html = "";
                html += "<option value=''>Filtrar por lugar</option>";
                $.each(response,function(key,item){
                    html += "<option value='"+item.idlugar+"'>"+item.nombre+"</option>";
                });                    	
                document.getElementById("lugar").innerHTML = html;
            },
            error: function(xhr, status, error) {
                alert(xhr.responseText);
            }
        });
    }