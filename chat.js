$(document).on("ready", empezarChat);

window.intervalos = [];

function empezarChat(){
	console.log("Ejecutando empezar chat");
	window.online = false;
	//Verifico si estoy loggeado o no:
	$.ajaxSetup({
                    type: "POST",
                    data: {},
                    dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
                });
	$.ajax({
		url : "http://api.local/Clientes/estado",
		type: "POST",
		data: {},
		dataType:"json",
		success:function(response){
			if (response){ //estoy loggeado
				console.log(" estoy loggeado");
				$("#containerdelchat").css("visibility", "visible");
				actualizarestado(); //esto se fija si estoy en algun chat o no.
			}	
			else{ //No estoy loggeado
				console.log("No estoy loggeado");
				//$("#containerdelchat").html("");
				$("#containerdelchat").css("visibility", "hidden");
				//console.log("No se ejecuta ningun script de chat porque no estoy loggeado.");
			}
		},
		error: function(xhr, status, error) {	
	        alert(xhr.responseText);
	        return false;
		}
	});
}

function htmlchat(idsala,expira){ //Cargar con los datos default y luego se llaman los metodos que realizan los cambios
	//htmlchatbox = '<p id="estadosala"><span id="cargando">Cargando...</span></p>';
	console.log("ejecutando htmlchat "+idsala+" "+expira)
        /*
        <div class="chatbox__body__message chatbox__body__message--right">

            <div class="chatbox_timing">
                <ul>
                    <li><a href="#"><i class="fa fa-calendar"></i> 22/11/2018</a></li>
                    <li><a href="#"><i class="fa fa-clock-o"></i> 7:00 PM</a></a></li>
                </ul>
            </div>

            <img src="https://www.gstatic.com/webp/gallery/2.jpg" alt="Picture">
            <div class="clearfix"></div>
            <div class="ul_section_full">
                <ul class="ul_msg">
                    <li><strong>Info</strong></li>
                    <li>Cargando informacion de la sala...</li>
                </ul>
                <div class="clearfix"></div>
                <ul class="ul_msg2">
                    <li><a href="#"><i class="fa fa-pencil"></i> </a></li>
                    <li><a href="#"><i class="fa fa-trash chat-trash"></i></a></li>
                </ul>
            </div>
        </div>
    */
    htmlchatbox =    '<div class="chatbox__body__message chatbox__body__message--left">';
    /*
    htmlchatbox +=        '<div class="chatbox_timing">';
    htmlchatbox +=            '<ul>';
    htmlchatbox +=                '<li><a href="#"><i class="fa fa-calendar"></i> 22/11/2018</a></li>';
    htmlchatbox +=                '<li><a href="#"><i class="fa fa-clock-o"></i> 7:00 PM</a></a></li>';
    htmlchatbox +=            '</ul>';
    htmlchatbox +=        '</div>';
	*/
    //htmlchatbox +=        '<img src="https://www.gstatic.com/webp/gallery/2.jpg" alt="Picture">';
    htmlchatbox +=        '<div class="clearfix"></div>';
    htmlchatbox +=        '<div class="ul_section_full">';
	htmlchatbox +=             '<ul class="ul_msg">';
    htmlchatbox +=                '<li><strong>INFO</strong></li>';
    htmlchatbox +=                '<li>Espere mientras se cargan los mensajes de la sala... </li>';
    htmlchatbox +=            '</ul>';
    htmlchatbox +=            '<div class="clearfix"></div>';
    
    htmlchatbox +=            '<ul class="ul_msg2">';
    htmlchatbox +=                '<li><a href="#"><i class="fa fa-pencil"></i> </a></li>';
    htmlchatbox +=                '<li><a href="#"><i class="fa fa-trash chat-trash"></i></a></li>';
    htmlchatbox +=           ' </ul>';
    htmlchatbox +=        '</div>';
    htmlchatbox +=    '</div>';


	//htmlchatbox = '<div id="datossala">';
	//htmlchatbox += 'Cargando informacion de la sala...';
	//htmlchatbox += '</div><br />';

	/*
	htmlchatbox += '<p>';
	htmlchatbox += '<div style="width:450px;height:250px;overflow:hidden;padding:10px;border:1px double #DEBB07;" id="historial" class="historial">';
	htmlchatbox += 'Espere mientras se inicia la carga de mensajes...';
	htmlchatbox += '</div></p><br/>';
	


	htmlchatbox += '<div id="mensajebox">';
	htmlchatbox += '<input type="text" name="mensaje" id="mensaje" value="Mensaje" />';
	htmlchatbox +=  '<br><br><button onclick="enviarmensaje('+idsala+',mensaje)">Enviar Mensaje</button>';

	htmlchatbox +=  '<br><br><button onclick="cerrarsala('+idsala+')">Cerrar sala de chat</button>';
	//htmlchatbox +=  '<br><br><button onclick="actualizarestado()">Ver estado</button>';
	*/

	$("#crucesita.chatbox__title__close").css("visibility", "visible");
	$("#bodydelchat").html(htmlchatbox);
	$("body").get(0).style.setProperty("--orange", "#4CAF50");
	$("#titulodelchat").html("Sala "+idsala);
	$("#crucesita").click(function(){cerrarsala(idsala)});
	obtenermensajes(idsala);
	cargarDatosSala();
	$("#btn-chat").click(function(){enviarmensaje(idsala)});

	limpiarIntervalos(intervalos);
	intervalos.push(setInterval('obtenermensajes('+idsala+')', 4000));
	intervalos.push(setInterval('cargarDatosSala()', 4000));
	

}

function cargarDatosSala(){
	$.ajaxSetup({
		type: "POST",
		data: {},
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true
	});
	$.ajax({
		url : "http://api.local/Chat/online",
		type: "POST",
		data: {},
		dataType:"json",
		success:function(response){	
				if(response['online'] == true){
					$("#titulodelchat").html("Sala "+response['idsala']);
					/*
					console.log("Idsala : "+response['idsala']);
					htmlsala = 'Id sala: '+response['idsala']+'<br />';
					htmlsala += 'Expira : '+response['expira']+'';
					$("#datossala").html(htmlsala);*/
				}else{
					limpiarIntervalos(intervalos);
					console.log("Online : "+response['online']);
					$("body").get(0).style.setProperty("--orange", "#fd7e14");
					$("#crucesita.chatbox__title__close").css("visibility", "hidden");
					$("#titulodelchat").html("Ayuda");
					$("#btn-chat").attr("onclick", "").unbind("click");
					$("#empezarsala").click(function(){empezarSala()});
					
					/*
					texto =  '<br><br><button onclick="empezarSala()">Empezar a chatear</button>';
					$("#chatbox").html("");
					$("#chat").html(texto);*/
				}
			//console.log(response.online);
		},
		error: function(xhr, status, error) {	
	        alert(xhr.responseText);
	        return false;
		}
	});
}

function actualizarestado(){
	$.ajaxSetup({
		type: "POST",
		data: {},
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true
	});
	$.ajax({
		url : "http://api.local/Chat/online",
		type: "POST",
		data: {},
		dataType:"json",
		success:function(response){	
			console.log(response);
				if(response['online'] == true){

					$("#empezarsala").attr("onclick", "").unbind("click");
					//$("#crucesita").click(function(){cerrarsala(response['idsala'])});
					console.log("Idsala : "+response['idsala']);
					$("#chat").html("");
					htmlchat(response['idsala'],response['expira']); ////

				}else{
					console.log("Online : "+response['online']);
					$("body").get(0).style.setProperty("--orange", "#fd7e14");
					$("#crucesita.chatbox__title__close").css("visibility", "hidden");
					$("#titulodelchat").html("Ayuda");
					$("#btn-chat").attr("onclick", "").unbind("click");
					//$("#empezarsala").click(function(){empezarSala()});
				}
			//console.log(response.online);
		},
		error: function(xhr, status, error) {	
	        alert(xhr.responseText);
	        return false;
		}
	});
}

function empezarSala(){ 
	console.log("Quiero empezar a chatear.");
	$.ajaxSetup({
		type: "POST",
		data: {},
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true
	});
	$.ajax({
		url : "http://api.local/Chat/empezarSala",
		type: "POST",
		data: {},
		dataType:"json",
		success:function(response){
			console.log(response);
			if (response['exito'] == true){
				empezarChat();
			}else{
				alert(response['errores'].ERROR);
			}
		},
		error: function(xhr, status, error) {	
	        alert(xhr.responseText);
	        return false;
		}
	});
}

function obtenermensajes(idsala){
	//console.log("Quiero obtener los mensajes de la sala "+idsala);
	//$("#historial").html("Cargando mensajes...");
	htmlmensajes = "";
	$.ajaxSetup({
		type: "POST",
		data: {},
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true
	});
	$.ajax({
		url : "http://api.local/Chat/obtenerMensajes",
		type: "POST",
		data: {idsala:idsala},
		dataType:"json",
		success:function(response){
			console.log(response);


			if (response['exito'] == true){
				$.each(response['mensajes'],function(index,mensaje){
					if(mensaje['tipoautor'] == 'CLIENTE'){


						htmlmensajes +=' <div class="chatbox__body__message chatbox__body__message--right">';
						htmlmensajes +='<div class="clearfix"></div>';
    					htmlmensajes +='<div class="ul_section_full">';
						htmlmensajes +='<ul class="ul_msg">';
    					htmlmensajes +='<li><strong>Yo</strong></li>';


					}else if(mensaje['tipoautor'] == 'EMPLEADO'){


						htmlmensajes +='<div class="chatbox__body__message chatbox__body__message--left">';
						htmlmensajes +='<div class="clearfix"></div>';
    					htmlmensajes +='<div class="ul_section_full">';
						htmlmensajes +='<ul class="ul_msg">';
    					htmlmensajes +='<li><strong>Empleado</strong></li>';


					}else{


						htmlmensajes +='<div class="chatbox__body__message chatbox__body__message--left">';
						htmlmensajes +='<div class="clearfix"></div>';
    					htmlmensajes +='<div class="ul_section_full">';
						htmlmensajes +='<ul class="ul_msg">';
    					htmlmensajes +='<li><strong>Info</strong></li>';


					}
					htmlmensajes +=                '<li class="mensajito">'+mensaje['mensaje']+'</li>';
				    htmlmensajes +=            '</ul>';
				    htmlmensajes +=            '<div class="clearfix"></div>';
				    htmlmensajes +=            '<ul class="ul_msg2">';
				    htmlmensajes +=                '<li><a href="#"><i class="fa fa-pencil"></i> </a></li>';
				    htmlmensajes +=                '<li><a href="#"><i class="fa fa-trash chat-trash"></i></a></li>';
				    htmlmensajes +=           ' </ul>';
				    htmlmensajes +=        '</div>';
				    htmlmensajes +=    '</div>';
					//htmlmensajes += "<br>"+mensaje['mensaje']+"<br>"
					//console.log(mensaje['mensaje']);
				});
				$("#bodydelchat").html(htmlmensajes);
			}else{
				alert(response['errores'].ERROR);
				$("#crucesita").attr("onclick", "").unbind("click");
				$("#empezarsala").addClass("chatbox--tray");
				//$("#empezarsala").click(function(){empezarSala()});

			}
		},
		error: function(xhr, status, error) {	
	        alert(xhr.responseText);
	        return false;
		}
	});
}

function enviarmensaje(idsala){
	var mensaje = $("#btn-input").val();
	console.log("Quiero enviar un mensaje a "+idsala);
	console.log(mensaje);
	
	$.ajaxSetup({
		type: "POST",
		data: {},
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true
	});
	$.ajax({
		url : "http://api.local/Chat/enviarMensaje",
		type: "POST",
		data: {idsala:idsala,mensaje:mensaje},
		dataType:"json",
		success:function(response){
			//console.log(response); //Insertar mensajes en el box y actualizar instantaneamente
			if (response['exito'] == true){
				//mensaje.value = "";
				$("#btn-input").val("")
				obtenermensajes(idsala);
				cargarDatosSala();
			}else{
				alert(response['errores'].ERROR);
			}
		},
		error: function(xhr, status, error) {	
	        alert(xhr.responseText);
	        return false;
		}
	});
}

function cerrarsala(idsala){
	console.log("Quiero cerrar la sala de chat. "+idsala);
	$.ajaxSetup({
		type: "POST",
		data: {},
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true
	});
	$.ajax({
		url : "http://api.local/Chat/cerrarSala",
		type: "POST",
		data: {idsala:idsala},
		dataType:"json",
		success:function(response){
			console.log(response);
			if (response['exito'] == true){
				console.log("Cerró sala")
				$("#crucesita").attr("onclick", "").unbind("click");
				empezarChat();
				limpiarIntervalos(intervalos);
				$("#empezarsala").click(function(){empezarSala()});
			}else{
				alert(response['errores'].ERROR);
			}
		},
		error: function(xhr, status, error) {	
	        alert(xhr.responseText);
	        return false;
		}
	});
}

function limpiarIntervalos(intervals){
	intervals.forEach(clearInterval);
}

