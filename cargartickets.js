function cargartickets(tickets) {
	
	/*console.log("Tickets x fecha");
	console.log(tickets);*/
	console.log("--------------");
	console.log("Tickets:");
	console.log(tickets);
	console.log("--------------");

	cargarFechasxTickets(tickets);
	var e = document.getElementById("selecttickets");
	var fecha = e.options[e.selectedIndex].value;

	cargarVisual(tickets,fecha);
	$("#selecttickets").change(function(){
		fechaseleccionada = $(this).val(); //La fecha seleccionada
		cargarVisual(tickets,fechaseleccionada);
	});
}

function cargarVisual(tickets,fecha) {

	var htmltickets = "<div class='row'>";
	$.each(tickets,function(index,grupodetickets){
		if(grupodetickets.fecha == fecha){ //si la fecha de los tickets corresponde con la fecha seleccionada
			window.ticketscargados = grupodetickets;
			$.each(grupodetickets,function(clave,tipoticket){ //La clave es "fecha", para las fechas, o el index para los tickets, cuando la clave NO es fecha, entonces el valor es un tipo de ticket, sino es la fecha misma
				if(clave != "fecha"){
							//esto aca es un monton de logica para verificar la cantidad de tickets vendidos, los disponibles , y los ocupados, para los diferentes tipos de tickets
							var cantidad = tipoticket.cantidad;
							if(tipoticket.numerado == 0){
								var numerado = 0;
								if(tipoticket.vendidas.vacio){var vendidas = 0;}
								else{
									var vendidas = tipoticket.vendidas.cantidad;
								}
							}else{
								var numerado = 1;
								if(tipoticket.vendidas.vacio){
									var vendidas = 0;
									var ocupadas = ["Ninguno"];
								}else{
									count = tipoticket.vendidas.length;
									var vendidas = count;
									var ocupadas = [];
									$.each(tipoticket.vendidas,function(tipoticket,valores){
										ocupadas.push(valores.numero);
									});
									//console.log("Asientos ocupados "+ocupadas);
									/*var objeto = [];
									objeto['nombre'] = "contador"+contador;
									objeto['ocupadas'] = ocupadas;
									arrayocupados.push(objeto);
									tipoticket.ocupadas= ocupadas;*/
								}
							}

							var disponibles = cantidad - vendidas;
							tipoticket.disponibles= disponibles;

							//htmltickets +="Id ticket :"+tipoticket.idtipoticket+" <br>";
							htmltickets +="<div class='col-lg-4 selectorticket'>"
							htmltickets += "<h4>"+tipoticket.nombre+"</h4><hr>";
							//htmltickets += '<img src="http://cdn.local/imagenes/referenciatipoticket/'+tipoticket.referencia+'" alt="Ticket sin referencia" height="50" width="50"><br>';
							htmltickets +="Precio :"+tipoticket.precio+"$<br>";

							if(disponibles > 0){
								htmltickets +="<input type=number id=cantidadtickets"+tipoticket.idtipoticket+" value='0' min='0' max='"+disponibles+"' onchange='listenerTickets(this.id,"+tipoticket.idtipoticket+",this.value)'> <br>";
							}
							else{
								htmltickets +="Entradas agotadas<br>";
								htmltickets +="<input type=hidden id=cantidadtickets"+tipoticket.idtipoticket+" value='0' min='0' max='0'> <br>";								
							}
							if(tipoticket.numerado == 1){
								htmltickets +="Numerado<br>";
								htmltickets +="Numeros ocupados:<br>";
								htmltickets +="<textarea rows='1' cols='20' readonly>"+ocupadas+"</textarea> <br>"; 
								htmltickets += "<span id=numerosdeasiento"+tipoticket.idtipoticket+"></span>";
							}
							else{
								//htmltickets +="Numerado : No <br>";
							}
							htmltickets +="<hr>"
							htmltickets +="</div>"
							
						//console.log(tipoticket.idtipoticket);
						//console.log(tipoticket);
					}
				});
		}
	});
	htmltickets +="</div>"
	$("#formtickets").html(htmltickets);
	//console.log(arrayocupados);
}

function cargarFechasxTickets(tickets) {
	var fechas =""
	$.each(tickets,function(key,item){
		if(item.fecha){
			fechas +="<option value='"+item.fecha+"'>"+item.fecha+"</option>";
		}
	});
	$("#selecttickets").html(fechas);
}

function listenerTickets(idform,idtipoticket,cantidad){
	/*
	console.log(ticketscargados);*/
	//console.log(descuentoscargados);
	cantidad = parseInt(cantidad);
	var ticketseleccionado = seleccionarticketcargado(idtipoticket);

	if(ticketseleccionado != null){
		//console.log("Ticket seleccionado: ");console.log(ticketseleccionado);
		if (cantidad<0) {
			alert("La cantidad no puede ser menor a 0");
			document.getElementById(idform).value=0;
			if (ticketseleccionado.numerado==1) {
				elegirAsientosVisual(ticketseleccionado.idtipoticket,0,"numerosdeasiento"+ticketseleccionado.idtipoticket);
			}
		}else{
			//La siguiente variable me dice si me sobrepaso del stock, y por cuanta diferencia 
			//(0 Significa que todavia hay stock, mayor a 0 significa que sobre pasé el stock por la cantidad dada )
			var superastock = calcularstock(ticketseleccionado.idtipoticket,cantidad);

			if (superastock == 0){
				//console.log("Nueva cantidad de individuales : "+ticketseleccionado.individuales);
				if(cantidad == 0){
					delete ticketseleccionado.individuales;
				}
			}else{
				cantidad = cantidad-superastock;
				ticketseleccionado.individuales = cantidad;
				alert("Se está superando el stock admitido para el ticket ID "+idtipoticket+".\nSe supera por la cantidad de "+superastock+"\nNueva cantidad: "+ticketseleccionado.individuales);
				document.getElementById(idform).value=cantidad;
			}

			if (ticketseleccionado.numerado==1) {
				elegirAsientosVisual(ticketseleccionado.idtipoticket,cantidad,"numerosdeasiento"+ticketseleccionado.idtipoticket);
			}
		}
	}else{
		alert("No se reconoce el ticket seleccionado.");
	}
}

function calcularstock(idtipoticket,cantidad){
	cantidad = parseInt(cantidad);
	var ticketseleccionado = seleccionarticketcargado(idtipoticket);
	var disponibles = ticketseleccionado.disponibles;
	var cantidadelegidaspaquete = 0;
	if (ticketseleccionado.cantidadelegidaspaquete == undefined){
		//console.log("El tipo de ticket no tiene ordenes dentro de paquetes");
	}else{
		//console.log("El tipo de ticket TIENE ordenes dentro de paquetes");
		$.each(ticketseleccionado.cantidadelegidaspaquete,function(idpaquete,cantidadpedidas){
			cantidadelegidaspaquete += cantidadpedidas;
		});
	}
	/*
		console.log("Calculando el stock del ticket: "+idtipoticket);
		console.log("Para "+cantidad+" de tickets individuales más "+cantidadelegidaspaquete);
		console.log("La cantidad de disponibles es: "+disponibles);
	*/
	if (disponibles >= cantidad+cantidadelegidaspaquete){ 
		ticketseleccionado.individuales = cantidad;
		//console.log("Actualizando el stock del ticket");console.log(ticketseleccionado);
		return 0;
	}else{
		var diferencia = (cantidad+cantidadelegidaspaquete)-disponibles;
		//console.log("Actualizando el stock del ticket");console.log(ticketseleccionado);
		return diferencia;
	}
}

//Esta funcion inserta el codigo para poder elegir los numeros de asiento del tipo de ticket dado
//Pide el idtipoticket para verificar el stock(y los numeros de asiento), la cantidad para saber cuantos campos pone, y el span para saber donde los pone
function elegirAsientosVisual(idtipoticket,cantidad,span){

	/*
		Cada vez que cambio la cantidad de tickets que quiero comprar
		de los numerados se vacian todos los campos, asi que tambien se va
		a vaciar los numeros seleccionados sueltos
	*/
	cantidad = parseInt(cantidad);
	var ticketseleccionado = seleccionarticketcargado(idtipoticket);
	var elegidassueltas = new Object();
	ticketseleccionado.elegidassueltas = elegidassueltas;
	var htmltickets = "";
		for(i = 0; i < cantidad; i++) {
			htmltickets +=(i+1)+": <input type=number id=elegirAsiento"+idtipoticket+":"+i+" min='0' onchange='elegirAsiento(this.id,this.value,"+idtipoticket+")'> <br>";
		}
	$("#"+span).html(htmltickets);
}

function elegirAsiento(idform,numeroasiento,idtipoticket){
	numeroasiento = parseInt(numeroasiento);
	//console.log("Se esta seleccionando el numero de asiento "+numeroasiento+" para el ticket id "+idtipoticket+".");
	var ticketseleccionado = seleccionarticketcargado(idtipoticket);
	//evaluar si puedo realizar la siguiente accion de agregar el numero al array de seleccionados
	if(comprobarnumero(ticketseleccionado,numeroasiento)){
		ticketseleccionado.elegidassueltas[idform] = numeroasiento;
	}else{
		var valoranterior = ticketseleccionado.elegidassueltas[idform]; //despues va haber que determinar si el valor anterior es de un suelto o un paquete.

		document.getElementById(idform).value=valoranterior;
	}
	/*console.log("El ticket seleccionado tiene asientos elegidos, los voy a mostrar.");
	console.log(ticketseleccionado.elegidassueltas);*/
}

function comprobarnumero(tipoticket,numero){

	/*
		console.log("comprobarnumero");console.log(numero);
		console.log(tipoticket);
	*/
	numero = parseInt(numero);
	if(tipoticket.vendidas.vacio){
		//No esta ocupado el asiento seleccionado
		var ocupado = false;
	}else{
		var ocupado = false;
		$.each(tipoticket.vendidas,function(index,ticket){
			if (numero == ticket.numero) {
				ocupado = true;
			}
		});
		//si el resultado es true esta ocupado sino no
	}
	var cantidad = tipoticket.cantidad;
	if (numero>cantidad) {
		alert("El numero no puede superar el valor "+cantidad);
		return false;
	}
	if (numero<=0) {
		alert("El numero debe ser mayor a 0.");
		return false;
	}

	if (ocupado){
		alert("El asiento "+numero+" ya se encuentra ocupado");
		return false;
	}else{
		var seleccionado = false; //Esta variable indica si el numero de asiento ya fue seleccionado
		$.each(tipoticket.elegidassueltas,function(idform,numeroasiento){ //Primero cargo el ticket adecuado
			if(numero == numeroasiento){
				seleccionado =  true;
			}
		});
		if (tipoticket.elegidasPaquete == undefined){
			//console.log("El ticket no tiene marcadas de elegidas por paquete");
		}else{
			//console.log("El ticket tiene marcadas de elegidas por paquete");
			//console.log(tipoticket.elegidasPaquete);
			$.each(tipoticket.elegidasPaquete,function(idpaquete,elegidaspaquete){
				$.each(elegidaspaquete,function(idform,numeroasiento){
					if(numero == numeroasiento){
						seleccionado =  true;
					}
				});
			});
		}
		if (seleccionado) {
			alert("El asiento "+numero+" ya se encuentra seleccionado");
			return false;
		}else{
			return true;
		}
	}
	/*
	console.log("Numero de asiento a elegir "+numero);
	console.log("Ocupado = "+ocupado);
	console.log(tipoticket);
	*/
}

function seleccionarticketcargado(idtipoticket){
	ticketseleccionado = null
	$.each(ticketscargados,function(index,ticket){ 
		if(ticket.idtipoticket == idtipoticket){
			ticketseleccionado =  ticket;
		}
	});
	return ticketseleccionado;
}
