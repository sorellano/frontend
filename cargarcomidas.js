function cargarcomidas(comidas) {

	console.log("--------------");
	console.log("Comidas:");
	console.log(comidas);
	console.log("--------------");

	var html = "<div class='row'>";

	$.each(comidas,function(key,comida){
		html +="<div class='col-lg-4 comidadiv'>"
		html += "<h4>"+comida.nombrecomida+"</h4><hr>Precio:"+comida.precio+"$. &nbsp;<br>";
		html +='<img src="http://cdn.local/imagenes/imagencomida/'+comida.imagen+'" alt=" " height="60px" width="80px"><br>';
		html += "<br>";
		html += "<input type=number id=comida"+comida.idcomida+" value='0' min='0' max='99' onchange='listenerComida(this.id,"+comida.idcomida+",this.value)'>"
		html += "<hr></div>";
	});
	html += "</div>";
	window.comidas = comidas; //Se cargan todas las comidas porque no difieren de metodo de pago o fecha como es el caso de los tickets y paquetes
	$("#formcomidas").html(html);
}

function listenerComida(idform,idcomida,cantidad) {
 	cantidad = parseInt(cantidad);
	var comidaseleccionada = null;
	$.each(comidas,function(index,comida){
		if(comida.idcomida == idcomida){
			comidaseleccionada =  comida;
		}
	});
	if(comidaseleccionada){
		if (comidaseleccionada.baja == 1) {
			alert("La comida seleccionada parece estar dada de baja.");
		}else{
			if (cantidad<0){ 
				alert("La cantidad no puede ser menor a 0.");
				document.getElementById(idform).value=0;
				comidaseleccionada.cantidad= 0;
			}
			else{
				if (cantidad>99){
						alert("No se pueden ordenar mas de 99 unidades.");
						document.getElementById(idform).value=99;
						comidaseleccionada.cantidad= 99;
				}else{
					comidaseleccionada.cantidad= cantidad;
				}
			}	
		}
	}else{
		alert("No se reconoce la comida modificada");
	}
}
