$(document).on("ready", llenarcarousel);
function llenarcarousel(){
	$.ajaxSetup({
                    type: "POST",
                    data: {},
                    dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
            });
	$.ajax({
		url : "http://api.local/eventos/carousel",
		type: "POST",
		data: {},
		dataType:"json",
		success:function(response){

			var cantidadindicators = Math.ceil(response.totalregistros/4);

			var html = '';
			var htmlindicators = '';
			var primerlinea = true;
			var evento
			var totalregistros = response.totalregistros
			var contador = 0;
			for (var i = cantidadindicators-1; i >= 0; i--) {
				if (primerlinea){
					html += '<div class="carousel-item active">';
					primerlinea = false;
				}else{
					html += '<div class="carousel-item">';
				}

				html += '<div class="cuatrodivisiones">';

				html += '<div class="row">';
				if(contador<totalregistros){
					evento = response.eventos[contador];
					html += '<div class="col-lg" id="imghover">';
					if(evento.imagen == null){evento.imagen = "Sinimagendeevento.jpg";} //Valor de imagen default
					html += '<img class="d-block w-100 imagencarousel" src="http://cdn.local/imagenes/imagenevento/'+evento.imagen+'" alt="http://cdn.local/imagenes/imagenevento/Sinimagendeevento.jpg">';
					html += '<div class="overlay">';
					html += '<div class="text">';
					html += evento.nombreevento;
					html += '<br>';
					html += evento.nombrelugar;
					html += '<br>';
					html += '<a href="/evento.html?idevento='+evento.idevento+'">';
					html += '<button type="button" class="btn btn-outline-secondary">Ir al evento</button></a>';
					html += '</div>'; //Text
					html += '</div>'; //Overlay
					html += '</div>'; //Col-lg imghover
					contador ++;	
				}
				if(contador<totalregistros){
					evento = response.eventos[contador];
					html += '<div class="col-lg" id="imghover">';
					if(evento.imagen == null){evento.imagen = "Sinimagendeevento.jpg";}
					html += '<img class="d-block w-100 imagencarousel" src="http://cdn.local/imagenes/imagenevento/'+evento.imagen+'" alt="Imagen del carousel">';
					html += '<div class="overlay">';
					html += '<div class="text">';
					html += evento.nombreevento;
					html += '<br>';
					html += evento.nombrelugar;
					html += '<br>';
					html += '<a href="/evento.html?idevento='+evento.idevento+'">';
					html += '<button type="button" class="btn btn-outline-secondary">Ir al evento</button></a>';
					html += '</div>'; //Text
					html += '</div>'; //Overlay
					html += '</div>'; //Col-lg imghover
					contador ++;
				}
				html += '</div>'; //row1

				html += '<div class="row">';
				if(contador<totalregistros){
					evento = response.eventos[contador];
					html += '<div class="col-lg" id="imghover">';
					if(evento.imagen == null){evento.imagen = "Sinimagendeevento.jpg";}
					html += '<img class="d-block w-100 imagencarousel" src="http://cdn.local/imagenes/imagenevento/'+evento.imagen+'" alt="Imagen del carousel">';
					html += '<div class="overlay">';
					html += '<div class="text">';
					html += evento.nombreevento;
					html += '<br>';
					html += evento.nombrelugar;
					html += '<br>';
					html += '<a href="/evento.html?idevento='+evento.idevento+'">';
					html += '<button type="button" class="btn btn-outline-secondary">Ir al evento</button></a>';
					html += '</div>'; //Text
					html += '</div>'; //Overlay
					html += '</div>'; //Col-lg imghover
					contador ++;
				}
				if(contador<totalregistros){
					evento = response.eventos[contador];
					html += '<div class="col-lg" id="imghover">';
					if(evento.imagen == null){evento.imagen = "Sinimagendeevento.jpg";}
					html += '<img class="d-block w-100 imagencarousel" src="http://cdn.local/imagenes/imagenevento/'+evento.imagen+'" alt="Imagen del carousel">';
					html += '<div class="overlay">';
					html += '<div class="text">';
					html += evento.nombreevento;
					html += '<br>';
					html += evento.nombrelugar;
					html += '<br>';
					html += '<a href="/evento.html?idevento='+evento.idevento+'">';
					html += '<button type="button" class="btn btn-outline-secondary">Ir al evento</button></a>';
					html += '</div>'; //Text
					html += '</div>'; //Overlay
					html += '</div>'; //Col-lg imghover
					contador ++;
				}
				html += '</div>'; //row2

				html += '</div>'; //cuatro divisiones
				html += '</div>'; //carousel item

			}
			$(".carousel-inner").html(html);
			var primerlinea = true;
			for (var i = 0; i <= cantidadindicators - 1; i++) {
				if (primerlinea){
					htmlindicators += '<li data-target="#carousel" data-slide-to="'+i+'" class="active"></li>';
					primerlinea = false;
				}else{
					htmlindicators += '<li data-target="#carousel" data-slide-to="'+i+'"></li>';
				}
			}
			$(".carousel-indicators").html(htmlindicators);
		},
		error: function(xhr, status, error) {
            alert(xhr.responseText);
        }
	});
}