$(document).on("ready", main);


function main(){
	//
	$("input[name=busqueda]").keyup(function(){
		textobuscar = $(this).val();
		//valoroption = $("#cantidad").val();
		valorlugar = $("#lugar").val();
		valorcategoria = $("#categoria").val();
		mostrarDatos(textobuscar,1,valorlugar,valorcategoria);
	});

	$("body").on("click",".paginacion li a",function(e){
		e.preventDefault();
		valorhref = $(this).attr("href");
		valorBuscar = $("input[name=busqueda]").val();
		valorlugar = $("#lugar").val();
		valorcategoria = $("#categoria").val();
		//valoroption = $("#cantidad").val();
		mostrarDatos(valorBuscar,valorhref,valorlugar,valorcategoria);
	});
	
	$("#lugar").change(function(){
		valorlugar = $(this).val();
		valorcategoria = $("#categoria").val();
		valorBuscar = $("input[name=busqueda]").val();
		mostrarDatos(valorBuscar,1,valorlugar,valorcategoria);
	});

	$("#categoria").change(function(){
		valorcategoria = $(this).val();
		valorBuscar = $("input[name=busqueda]").val();
		valorlugar = $("#lugar").val();
		mostrarDatos(valorBuscar,1,valorlugar,valorcategoria);
	});
}

function mostrarDatos(valorBuscar,pagina,lugar,categoria){
	$.ajaxSetup({
                    type: "POST",
                    data: {},
                    dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
            });
	$.ajax({
		url : "http://api.local/eventos/listar",
		type: "POST",
		data: {buscar:valorBuscar,nropagina:pagina,lugar:lugar,categoria:categoria},
		dataType:"json",
		success:function(response){
			eventos = response.eventos;
			cantidadeventos = eventos.length;
			//console.log(cantidadeventos);
			var cantidadrows = Math.ceil(cantidadeventos/3);
			//console.log(cantidadrows);
			var totalregistros = response.totalregistros
			var contador = 0;
			var html = '';
			for (var i = cantidadrows - 1; i >= 0; i--) {
				//console.log("row nro "+i);
				html += '<div class="row">';
				if(contador<cantidadeventos){
					html += '\n';
					evento = response.eventos[contador];
					//console.log(evento);
					html += '<a href="/evento.html?idevento='+evento.idevento+'" style="text-decoration: none;color: black;">';
					html += '<div class="col-lg proximasfunciones">'
					if(evento.imagen == null){evento.imagen = "Sinimagendeevento.jpg";} //Valor de imagen default
					html += '<img class="d-block w-100 imagenproximasfunciones" src="http://cdn.local/imagenes/imagenevento/'+evento.imagen+'" alt="Imagen">';
					html +='<div class="proximasfuncionescaption">'
					html +=    '<h5>'+evento.nombreevento+'</h5>'
					html +=    '<p>'+evento.nombrelugar+'</p>'
					html +=    '<p>'+evento.fechas[0].fecha+'</p>';
					html +=  '</div>'
					html += '</a>';
					html += '</div>';
					contador++;
				}
				if(contador<cantidadeventos){
					html += '\n';
					evento = response.eventos[contador];
					//console.log(evento);
					html += '<a href="/evento.html?idevento='+evento.idevento+'" style="text-decoration: none;color: black;">';
					html += '<div class="col-lg proximasfunciones">'
					if(evento.imagen == null){evento.imagen = "Sinimagendeevento.jpg";} //Valor de imagen default
					html += '<img class="d-block w-100 imagenproximasfunciones" src="http://cdn.local/imagenes/imagenevento/'+evento.imagen+'" alt="Imagen">';
					html +='<div class="proximasfuncionescaption">'
					html +=    '<h5>'+evento.nombreevento+'</h5>'
					html +=    '<p>'+evento.nombrelugar+'</p>'
					html +=    '<p>'+evento.fechas[0].fecha+'</p>';
					html +=  '</div>'
					html += '</a>';
					html += '</div>';
					contador++;
				}
				if(contador<cantidadeventos){
					html += '\n';
					evento = response.eventos[contador];
					//console.log(evento);
					html += '<a href="/evento.html?idevento='+evento.idevento+'" style="text-decoration: none;color: black;">';
					html += '<div class="col-lg proximasfunciones">';
					if(evento.imagen == null){evento.imagen = "Sinimagendeevento.jpg";} //Valor de imagen default
					html += '<img class="d-block w-100 imagenproximasfunciones" src="http://cdn.local/imagenes/imagenevento/'+evento.imagen+'" alt="Imagen">';
					html +='<div class="proximasfuncionescaption">';
					html +=    '<h5>'+evento.nombreevento+'</h5>';
					html +=    '<p>'+evento.nombrelugar+'</p>';
					html +=    '<p>'+evento.fechas[0].fecha+'</p>';
					html +=  '</div>'
					html += '</a>';
					html += '</div>';
					contador++;
				}
				html += '</div>';
			}
			$(".proximoseventos").html(html);


			// ------------------------------------Paginador

			linkseleccionado = Number(pagina);
			//total registros
			totalregistros = response.totalregistros;

			numerolinks = Math.ceil(totalregistros/20);
			paginador = "<ul class='pagination'>";
			if(linkseleccionado>1)
			{
				paginador+="<li><a href='1'>&laquo;</a></li>";
				paginador+="<li><a href='"+(linkseleccionado-1)+"' '>&lsaquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&laquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&lsaquo;</a></li>";
			}
			//muestro de los enlaces 
			//cantidad de link hacia atras y adelante
 			cant = 3;
 			//inicio de donde se va a mostrar los links
			pagInicio = (linkseleccionado > cant) ? (linkseleccionado - cant) : 1;
			//condicion en la cual establecemos el fin de los links
			if (numerolinks > cant)
			{
				//conocer los links que hay entre el seleccionado y el final
				pagRestantes = numerolinks - linkseleccionado;
				//defino el fin de los links
				pagFin = (pagRestantes > cant) ? (linkseleccionado + cant) :numerolinks;
			}
			else 
			{
				pagFin = numerolinks;
			}

			for (var i = pagInicio; i <= pagFin; i++) {
				if (i == linkseleccionado)
					paginador +="<li class='active'><a href='javascript:void(0)'>"+i+"</a></li>";
				else
					paginador +="<li><a href='"+i+"'>"+i+"</a></li>";
			}
			//condicion para mostrar el boton sigueinte y ultimo
			if(linkseleccionado<numerolinks)
			{
				paginador+="<li><a href='"+(linkseleccionado+1)+"' >&rsaquo;</a></li>";
				paginador+="<li><a href='"+numerolinks+"'>&raquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&rsaquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&raquo;</a></li>";
			}
			
			paginador +="</ul>";
			$(".paginacion").html(paginador);

			// ------------------------------------Paginador

		}
	});
}