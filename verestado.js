$(document).on("ready", verestado);

function verestado(){

	var sinLogin = ["/olvidelacontrasena.html", "/registro.html"]; //Las URL que se pueden acceder solamente cuando no estas loggeado.
	var conLogin = ["/perfil.html"]; //Las URL que se pueden acceder solamente cuando estas loggeado.

	$.ajaxSetup({
                    type: "POST",
                    data: {},
                    dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
                });
	$.ajax({
		url : "http://api.local/Clientes/estado",
		type: "POST",
		data: {},
		dataType:"json",
		success:function(response){
				if (response){
					if (jQuery.inArray(window.location.pathname, sinLogin) == -1) { //Si mi URL es diferente de las url sinLogin
						//Estoy en una URL permitida
					}
					else{
						//alert('URL para usuarios no loggeados');
						window.location.href = "/eventos.html"; //Sino lo mando a eventos
					}	
				}	
				else{
					if (jQuery.inArray(window.location.pathname, conLogin) == -1) { //Si mi URL es diferente de las url conLogin
						//Estoy en una URL permitida
					}
					else{
						//alert('URL para usuarios loggeados');
						window.location.href = "/eventos.html";
						
					}
				}
		},
		error: function(xhr, status, error) {	
	        alert("Error al intentar ver el estado del usuario.".xhr.responseText);
		}
	});
}