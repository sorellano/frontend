$(document).on("ready", categorias);
	function categorias() {
       	$.ajaxSetup({
            type: "POST",
            data: {},
            dataType: 'json',
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true
            });
        $.ajax({
            url : "http://api.local/categoria/listar",
            type: "POST",
            data: {},
            dataType:"json",
            success:function(response){

                /*
                    console.log("--------------");
                    console.log("Categorias:");
                    console.log(response);
                    console.log("--------------");
                */
                var html = "";
                 html += "<option value=''>Filtrar por categoria</option>"
                $.each(response,function(key,item){
                    html += "<option value='"+item.idcategoria+"'>"+item.nombre+"</option>";
                });                    	
                document.getElementById("categoria").innerHTML = html;
            },
            error: function(xhr, status, error) {
                alert(xhr.responseText);
            }
        });
    }