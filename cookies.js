function readCookie(nombre) {
    var text=RegExp(""+nombre+"[^;]+").exec(document.cookie);
    return decodeURIComponent(!!text ? text.toString().replace(/^[^=]+./,"") : "");
}

function deleteCookie(nombre) {
    document.cookie = nombre + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT';
}
