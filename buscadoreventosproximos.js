$(document).on("ready", proximoseventos);
function proximoseventos(){
	$.ajaxSetup({
                    type: "POST",
                    data: {},
                    dataType: 'json',
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true
            });
	$.ajax({
		url : "http://api.local/eventos/proximos",
		type: "POST",
		data: {},
		dataType:"json",
		success:function(response){
			//console.log(response);
			var cantidadrows = Math.ceil(response.totalregistros/3);
			var totalregistros = response.totalregistros
			var contador = 0;
			var html = '';
			for (var i = cantidadrows - 1; i >= 0; i--) {
				//console.log("row nro "+i);
				html += '<div class="row">';
				if(contador<totalregistros){
					html += '\n';
					evento = response.eventos[contador];
					html += '<a href="/evento.html?idevento='+evento.idevento+'" style="text-decoration: none;color: black;">';
					html += '<div class="col-lg proximasfunciones">'
					if(evento.imagen == null){evento.imagen = "Sinimagendeevento.jpg";} //Valor de imagen default
					html += '<img class="d-block w-100 imagenproximasfunciones" src="http://cdn.local/imagenes/imagenevento/'+evento.imagen+'" alt="Imagen">';
					html +='<div class="proximasfuncionescaption">'
					html +=    '<h5>'+evento.nombreevento+'</h5>'
					html +=    '<p>'+evento.fecha+'</p>'
					html +=  '</div>'
					html += '</a>';
					html += '</div>'
					contador++;
				}
				if(contador<totalregistros){
					html += '\n';
					evento = response.eventos[contador];
					html += '<a href="/evento.html?idevento='+evento.idevento+'" style="text-decoration: none;color: black;">';
					html += '<div class="col-lg proximasfunciones">'
					if(evento.imagen == null){evento.imagen = "Sinimagendeevento.jpg";} //Valor de imagen default
					html += '<img class="d-block w-100 imagenproximasfunciones" src="http://cdn.local/imagenes/imagenevento/'+evento.imagen+'" alt="Imagen">';
					html +='<div class="proximasfuncionescaption">'
					html +=    '<h5>'+evento.nombreevento+'</h5>'
					html +=    '<p>'+evento.fecha+'</p>'
					html +=  '</div>'
					html += '</a>';
					html += '</div>'
					contador++;
				}
				if(contador<totalregistros){
					html += '\n';
					evento = response.eventos[contador];
					html += '<a href="/evento.html?idevento='+evento.idevento+'" style="text-decoration: none;color: black;">';
					html += '<div class="col-lg proximasfunciones">'
					if(evento.imagen == null){evento.imagen = "Sinimagendeevento.jpg";} //Valor de imagen default
					html += '<img class="d-block w-100 imagenproximasfunciones" src="http://cdn.local/imagenes/imagenevento/'+evento.imagen+'" alt="Imagen">';
					html +='<div class="proximasfuncionescaption">'
					html +=    '<h5>'+evento.nombreevento+'</h5>'
					html +=    '<p>'+evento.fecha+'</p>'
					html +=  '</div>'
					html += '</a>';
					html += '</div>'
					contador++;
				}
				html += '</div>';
			}
			//alert(html);
			$(".proximoseventos").html(html);
		},
		error: function(xhr, status, error) {
            alert(xhr.responseText);
        }
	});
}