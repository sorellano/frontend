
function paquetes(descuentos,metododepago,fecha) {
	/*
		console.log("--------------");
		console.log("Paquetes:");
		console.log(descuentos);
		console.log("--------------");
	*/
	setTimeout(function() {
		var fechahtml = document.getElementById("selecttickets");
		var fecha = fechahtml.options[fechahtml.selectedIndex].value;
		var metododepagohtml = document.getElementById("metodosdepago");
		var metododepago = metododepagohtml.options[metododepagohtml.selectedIndex].value;
		cargarpaquetes(descuentos,fecha,metododepago);
		
	}, 550);

	$("#selecttickets").change(function(){
		fecha = $(this).val();
		var metododepagohtml = document.getElementById("metodosdepago");
		var metododepago = metododepagohtml.options[metododepagohtml.selectedIndex].value;
		cargarpaquetes(descuentos,fecha,metododepago);
	});

	$("#metodosdepago").change(function(){
		metododepago = $(this).val();
		var fechahtml = document.getElementById("selecttickets");
		var fecha = fechahtml.options[fechahtml.selectedIndex].value;
		cargarpaquetes(descuentos,fecha,metododepago);
	});
}

function cargarpaquetes(descuentos,fecha,metododepago) {
	paquetescargar = [];
	$.each(descuentos,function(index,paquete){
		var paquetebaja = paquete.baja; 
		var paquetefecha = paquete.fecha;
		var paqueteidevento = paquete.idevento;
		var metodosaceptados = paquete.metodosdepago;
		var coincidemetodo = 0;
		$.each(metodosaceptados,function(posicion,metodo){
			if(metodo.idmetodopago == metododepago){
				coincidemetodo = 1;
			}
		});
		if (paquetebaja == 0 && paquetefecha == fecha && paqueteidevento == idevento && coincidemetodo == 1){
			/*
				console.log("idpaquete = "+paquete.idpaquete);
				Evaluo el stock del paquete que coincide con la fecha,
				el metodo de pago, y el evento, y no esta dado de baja,
			 	para evitar el error de buscar valores de tickets que no se encuentren cargados.
			*/
			var stock = stockPaquetes(paquete); //Obtengo el stock del paquete para saber si lo quiero cargar o no.
			if (stock>0) {
				paquetescargar.push(paquete);
			}
		}
	});
	window.descuentoscargados = paquetescargar;
	agregarhtmlpaquetes(paquetescargar);
	

	
}

function listenerPaquete(cantidad,idform,idpaquete){
	cantidad = parseInt(cantidad);
	var paqueteseleccionado = null;
	$.each(descuentoscargados,function(index,paquete){
		if(paquete.idpaquete == idpaquete){
			paqueteseleccionado =  paquete;
		}
	});
	var stock = stockPaquetes(paqueteseleccionado);

	var numerados = ticketsNumerados(paqueteseleccionado.tipotickets);
	var todos = todosLosTicketsDelPaquete(paqueteseleccionado.tipotickets);

	if(paqueteseleccionado){
		if (paqueteseleccionado.baja == 1) {
			alert("El paquete elegido parece estar dado de baja");
		}else{
			if (cantidad<0) {
				alert("La cantidad no puede ser menor a 0.");
				document.getElementById(idform).value=0;
				agregarhtmltickets(numerados,0,idpaquete);
				paqueteseleccionado.cantidadelegida=0;
			}else{
				if (cantidad>stock){
					if (stock == 0) {
						alert("No hay mas stock del paquete seleccionado.");
					}else{
						alert("Solamente quedan : "+stock+" unidades.");
					}
					/*
						Cuando me queda stock pero pido una cantidad superior al stock,
						le advierto al usuario que solamente queda cierta cantidad de stock 
						y cambio el valor del form y el HTML para la cantidad adecuada.
					*/
					document.getElementById(idform).value=stock;
					agregarhtmltickets(numerados,stock,idpaquete);
					marcarElegidasPaquete(todos,stock,idpaquete);
					paqueteseleccionado.cantidadelegida=stock;
				}else{	
					agregarhtmltickets(numerados,cantidad,idpaquete);
					marcarElegidasPaquete(todos,cantidad,idpaquete);
					paqueteseleccionado.cantidadelegida=cantidad;
				}
			}
		}
	}else{
		alert("No se reconoce el paquete modificado");
	}
}

function ticketsNumerados(ticketsdelpaquete){
	var tickets = [];
	$.each(ticketsdelpaquete,function(index,tipoticket){ //Por cada ticket del paquete
		var idtipoticket = tipoticket.idtipoticket;
		//console.log(idtipoticket);
		var ticketseleccionado = seleccionarticketcargado(idtipoticket); //selecciono el ticket cargado correspondiente
		//console.log("El ticket seleccionado es ");console.log(ticketseleccionado);
		if (ticketseleccionado.numerado==1) {
			var ticket = new Object();
			ticket["idtipoticket"]=ticketseleccionado.idtipoticket;
			ticket["cantidadporpaquete"]=tipoticket.cantidad;
			tickets.push(ticket);
			}
		});
	return tickets;
}

function todosLosTicketsDelPaquete(ticketsdelpaquete){
	var tickets = [];
	$.each(ticketsdelpaquete,function(index,tipoticket){
		var idtipoticket = tipoticket.idtipoticket;
		var ticketseleccionado = seleccionarticketcargado(idtipoticket);
		var ticket = new Object();
		ticket["idtipoticket"]=ticketseleccionado.idtipoticket;
		ticket["cantidadporpaquete"]=tipoticket.cantidad;
		tickets.push(ticket);
		});
	return tickets;
}

function stockPaquetes(paquete){
	/*
		La cantidad maxima de paquetes que puedo pedir,
		 la calculo en base a la cantidad de 
		 tickets disponibles menos la cantidad ya ordenada de dicho ticket dividido por la cantidad por paquete de dicho ticket, 
		 para cada ticket contenido en el paquete.
		 Luego comparo todos los resultados y obtengo el limite mas bajo
	*/
	var stock=0;
	var valoresmaximos = []; //guardo un array con la cantidad maxima de paquetes que puedo pedir con respecto de cada ticket que tiene contenido individualmente
	var ticketsdelpaquete = paquete.tipotickets;
	$.each(ticketsdelpaquete,function(index,tipoticket){
		var idtipoticket = tipoticket.idtipoticket;

		var ticketseleccionado = seleccionarticketcargado(idtipoticket);

		if (ticketseleccionado == null){ //Evaluo que exista el ticket entre los cargados
			alert("El ticket ID:"+idtipoticket+" Del paquete  "+paquete.idpaquete+" no se encuentra cargado entre los ticketscargados.");	
		}else{

			var disponibles = ticketseleccionado.disponibles;
			var individuales = 0;
			var pedidasporpaquete = 0;

			if (ticketseleccionado.individuales == undefined){
				//console.log("El no ticket tiene individuales")
			}else{
				var individuales = ticketseleccionado.individuales;
			}
			if (ticketseleccionado.cantidadelegidaspaquete == undefined){
				//console.log("El tipo de ticket no tiene ordenes dentro de paquetes");
			}else{
				//console.log("El tipo de ticket TIENE ordenes dentro de paquetes");
				$.each(ticketseleccionado.cantidadelegidaspaquete,function(idpaquete,cantidadpedidas){
					if (paquete.idpaquete != idpaquete) {
						pedidasporpaquete += cantidadpedidas;
					}
				});
			}
			var cantidadmaxima = 0; //esta es la cantidad maxima de paquetes
			cantidadmaxima +=disponibles;
			cantidadmaxima -=individuales;
			cantidadmaxima -=pedidasporpaquete;
			cantidadmaxima /=tipoticket.cantidad;
			cantidadmaxima = Math.floor(cantidadmaxima);
			//console.log("cantidad de maxima de paquetes disponibles "+cantidadmaxima);
			//console.log(disponibles+"-"+individuales+"-"+pedidasporpaquete+"/"+tipoticket.cantidad+" = "+cantidadmaxima);
			valoresmaximos.push(cantidadmaxima);
		}
	});
	/*
		En valores maximos tengo la cantidad maxima que puedo pedir del paquete con respecto de cada ticket
		console.log(valoresmaximos);
		de esos valores maximos obtengo el menor de ellos
		Y con eso se cuanto es la cantidad maxima que puedo pedir del paquete en cuestion.
	*/
	stock = Math.min.apply(null, valoresmaximos);
	//console.log("Stock :  "+stock);
	paquete.stock=stock;
	//console.log("Stock del paquete "+paquete.idpaquete+" actualizado.");console.log(paquete);
	return stock;
}

function agregarhtmlpaquetes(paquetescargar) {
	htmlpaquetes = "<div class='row'>";
	$.each(paquetescargar,function(index,paquete){ 
		htmlpaquetes +="<div class='col-lg-4 paquetediv'>"
		htmltickets = "";
		htmlcomidas = "";
		//htmlpaquetes +="Id del paquete : "+paquete.idpaquete+" <br>";
		htmlpaquetes += "<h4>"+paquete.nombre+"</h4><hr>";
		htmlpaquetes +="<input type=number id='idpaquete"+paquete.idpaquete+"' value='0' min='0' onchange='listenerPaquete(this.value,this.id,"+paquete.idpaquete+")' ><br> <br>";
		//htmlpaquetes +='<img src="http://cdn.local/imagenes/paqueteimagen/'+paquete.imagen+'" alt="Paquete sin imagen" height="50" width="50"><br>';
		htmlpaquetes +="El paquete contiene los siguientes items : <br>";

		$.each(paquete.tipotickets,function(index,tipoticket){
			if(tipoticket.numerado == 0){
				var numerado = 0;
				if(tipoticket.vendidas.vacio){var vendidas = 0;}
				else{
					var vendidas = tipoticket.vendidas.cantidad;
				}
			}else{
				var numerado = 1;
				if(tipoticket.vendidas.vacio){
					var vendidas = 0;
					var ocupadas = ["Ninguno"];
				}else{
					count = tipoticket.vendidas.length;
					var vendidas = count;
					var ocupadas = [];
					$.each(tipoticket.vendidas,function(tipoticket,valores){
						ocupadas.push(valores.numero);
					});
				//console.log("Asientos ocupados "+ocupadas);
				/*var objeto = [];
				objeto['nombre'] = "contador"+contador;
				objeto['ocupadas'] = ocupadas;
				arrayocupados.push(objeto);
				tipoticket.ocupadas= ocupadas;*/
				}
			}
			//console.log(tipoticket);
			htmltickets +="<br> Ticket : "+tipoticket.nombre+"<br>";
			htmltickets +="Cantidad de unidades : "+tipoticket.cantidad+" <br>";
			htmltickets +="Con un descuento de : "+tipoticket.descuento+"% (Precio unitario "+tipoticket.precio+")<br>";
			if(tipoticket.numerado == 1){
				htmltickets +="Numerado : Si <br>";
				htmltickets +="Numeros ocupados: <br>";
				htmltickets +="<textarea rows='1' cols='20' readonly>"+ocupadas+"</textarea> <br>";
				htmltickets += "<span id=numerosdeasiento"+tipoticket.idtipoticket+"></span>";
				}
				else{
				htmltickets +="Numerado : No <br>";
			}
			htmltickets +="<hr>";
		});
		htmlpaquetes += htmltickets;

		$.each(paquete.comidas,function(index,comida){
			//console.log(comida);
			htmlcomidas +="<br> Comida : "+comida.nombre+"<br>";
			htmlcomidas +="Cantidad de unidades : "+comida.cantidad+" <br>";
			htmlcomidas +="Con un descuento de : "+comida.descuento+"% (Precio unitario "+comida.precio+")<hr><br>";

		});
		htmlpaquetes += htmlcomidas;
		
		htmlpaquetes +="<br> Precio total de cada paquete :<span id=precio> "+paquete.precio+"</span><br>";
		htmlpaquetes += "<span id=itemspaquete"+paquete.idpaquete+"></span><br><hr>";
		htmlpaquetes +="</div>"
	});
	htmlpaquetes += "</div>";
	$("#descuentos").html(htmlpaquetes);
}

function agregarhtmltickets(tickets,cantidad,idpaquete) {
	html = "";
	//console.log(tickets);
	cantidad = parseInt(cantidad);
	if (cantidad>0) {
		$.each(tickets,function(index,ticket){
			//console.log(ticket);
			html +="<br> Elegir asientos para el ticket : "+ticket.idtipoticket+" del paquete "+idpaquete+"<br>";
			var cantidaddeinputs = cantidad * ticket.cantidadporpaquete;
			for(i = 0; i < cantidaddeinputs; i++) {
				html += "<input type=number id=elegirAsientoPaquete"+ticket.idtipoticket+":"+i+" min='0' onchange='listenerAsientoPaquete(this.id,this.value,"+ticket.idtipoticket+","+idpaquete+")'> <br><br>";
			}
		});
	}
	//console.log(html);
	var span = "itemspaquete"+idpaquete;
	$("#"+span).html(html);
}

function marcarElegidasPaquete(tickets,cantidad,idpaquete) { //Esta funcion le indica al ticket cuantas unidades del mismo son pedidas desde el paquete.

	/*
		tickets son los tickets del paquete.
		cantidad de paquetes a pedir
		id paquete
	console.log(tickets);
	*/

	cantidad = parseInt(cantidad);
	$.each(tickets,function(index,tipoticket){ //Para cada ticket del paquete
		var idtipoticket = tipoticket.idtipoticket;

		var ticketseleccionado = seleccionarticketcargado(idtipoticket);
		//Ticketseleccionado es el ticket cargado.

		if (ticketseleccionado == null){ //Evaluo que exista el ticket entre los cargados
			alert("El ticket ID:"+idtipoticket+" Del paquete  "+idpaquete+" no se encuentra cargado entre los ticketscargados.");	
		}else{
			var cantidadelegidaspaquete = 0;
			cantidadelegidaspaquete = tipoticket.cantidadporpaquete*cantidad;

			if(cantidadelegidaspaquete > 0){
				if (ticketseleccionado.cantidadelegidaspaquete == undefined){
					var objeto = new Object();
					ticketseleccionado.cantidadelegidaspaquete = objeto;
				}
				ticketseleccionado.cantidadelegidaspaquete[idpaquete] = cantidadelegidaspaquete;
				
				if(ticketseleccionado.numerado == 1){
					if (ticketseleccionado.elegidasPaquete == undefined){
						ticketseleccionado.elegidasPaquete = new Object();
					}
					ticketseleccionado.elegidasPaquete[idpaquete] = new Object();
				}
			}else{
				delete ticketseleccionado.cantidadelegidaspaquete[idpaquete];
				if (typeof ticketseleccionado.elegidasPaquete !== 'undefined') {
				    delete ticketseleccionado.elegidasPaquete[idpaquete];
				}
				if (isEmpty(ticketseleccionado.cantidadelegidaspaquete)){
					delete ticketseleccionado.cantidadelegidaspaquete;
				}
				if (isEmpty(ticketseleccionado.elegidasPaquete)){
					delete ticketseleccionado.elegidasPaquete;
				}
			}
			
			

		}
		/*
			console.log("-----------------------------------------------");
			console.log("Se cambio elegidas paquete del siguiente ticket");
			console.log(ticketseleccionado);
		*/
	});	
}

function listenerAsientoPaquete(idform,numeroasiento,idtipoticket,idpaquete) {

	/*
		console.log("idform");console.log(idform);
		console.log("Numero de asiento elegido");console.log(numeroasiento);
		console.log("idtipoticket");console.log(idtipoticket);
		console.log("idpaquete");console.log(idpaquete);
	*/
	numeroasiento = parseInt(numeroasiento);
	var ticketseleccionado = seleccionarticketcargado(idtipoticket);
	if (ticketseleccionado == null){ //Evaluo que exista el ticket entre los cargados
		alert("El ticket ID:"+idtipoticket+" Del paquete  "+idpaquete+" no se encuentra cargado entre los ticketscargados.");	
	}else{
		if (ticketseleccionado.elegidasPaquete == undefined) {
			ticketseleccionado.elegidasPaquete = new Object();
			//console.log("Se creo la variable de elegidas por paquete.")
		}
		if(comprobarnumero(ticketseleccionado,numeroasiento)){
			/*
				Cada ticket cargado para el caso de los numerados tiene que tener una variable "elegidasPaquete"
				donde se guardan los numero de asiento seleccionados, agrupado por paquete
				Cada valor indica el form donde esta y el valor del asiento.
			*/
			if(ticketseleccionado.elegidasPaquete[idpaquete] == undefined){
				ticketseleccionado.elegidasPaquete[idpaquete] = new Object();
				//console.log("Se asocio el paquete al ticket. "+ticketseleccionado.idtipoticket);
			}
			var elegidasPaquete = ticketseleccionado.elegidasPaquete[idpaquete];
			elegidasPaquete[idform] = numeroasiento;
			//console.log("Elegida agregada");console.log(ticketseleccionado);
		}else{
			//Busco el valor anterior y lo inserto en el form
			var valoresanteriores = ticketseleccionado.elegidasPaquete[idpaquete];
			if(valoresanteriores == undefined){
				document.getElementById(idform).value=undefined;
			}
			else{
				var valoranterior = valoresanteriores[idform];
				document.getElementById(idform).value=valoranterior;
			}
		}
	}
}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}